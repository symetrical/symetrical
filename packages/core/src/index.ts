/**
 * @license
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://gitlab.com/symetrical/symetrical/blob/master/LICENSE
 */

/**
 * @module
 * @description Provides everything that is required to create the minium of a RESTfull API.
 */
export * from "./decorators";
export * from "./providers";
export * from "./interfaces";
