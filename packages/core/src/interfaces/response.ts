import * as restify from "restify";

export interface Response extends restify.Response {}
