import * as restify from "restify";

export interface Request extends restify.Request {}
