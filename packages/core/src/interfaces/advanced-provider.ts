/**
 * @license
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://gitlab.com/symetrical/symetrical/blob/master/LICENSE
 */

import {Type} from '../api';

/**
 * @description A "advanced provider" is actually just a specification
 * of the token that the injector should search for inside a constructor's parameters
 * and when it finds that token, it injects the specified dependecy.
 * So you can do things like customizing the framework things, like routing,
 * or some other things.
 */
export interface AdvancedProvider {
  key: any;
  dependency: Type<any>;
  containerKey: any;
  allowAccessTo: boolean;
}
