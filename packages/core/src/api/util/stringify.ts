/**
 * @license
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://gitlab.com/symetrical/symetrical/blob/master/LICENSE
 */

/**
 * @description Makes anything readable to console log it.
 * @param token 
 */
export function stringify(token: any): string {
  if (typeof token === "string") {
    return token;
  }

  if (token instanceof Array) {
    return "[" + token.map(stringify).join(", ") + "]";
  }

  if (token == null) {
    return "" + token;
  }

  if (token.overriddenName) {
    return `${token.overriddenName}`;
  }

  if (token.name) {
    return `${token.name}`;
  }

  const res = token.toString();

  if (res == null) {
    return "" + res;
  }

  const newLineIndex = res.indexOf("\n");
  return newLineIndex === -1 ? res : res.substring(0, newLineIndex);
}
