/**
 * @license
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://gitlab.com/symetrical/symetrical/blob/master/LICENSE
 */

import 'reflect-metadata';
import chalk from 'chalk';

import {stringify} from '../util';

import {Type} from '../interfaces/type';

import {DIContainer} from './di-container';
import {Dependency} from '../interfaces';

/**
 * @description The global abstract class that defines every Injector.
 */
export class Injector {
  public global: DIContainer;

  constructor() {
    this.global = new DIContainer();
  }

  /**
   * @returns The class constructor tokens
   * @param target The class to get the constructor tokens from
   */
  public resolveTokens<T>(target: Type<T>): Type<any>[] {
    return Reflect.getMetadata('design:paramtypes', target) || [];
  }

  /**
   * @returns The class constructor parameter names
   * @param target The class to get the parameters from
   */
  public resolveParameters<T>(target: Type<T>): string[] {
    return this.resolveTokens<T>(target).map(token => stringify(token));
  }

  /**
   * @returns The class constructor tokens injections
   * @param target The class to get the injections from
   */
  public resolveInjections<T>(target: Type<T>, containerKey?: any): any[] {
    const Parameters = this.resolveParameters(target);
    const Tokens = this.resolveTokens(target);

    const Injections = Tokens.map((token, i) => {
      if (typeof token === 'undefined') {
        throw new Error(
          `${chalk.redBright(
            chalk.bold('[ERROR]')
          )} Could not resolve the all the parameters for the class ${stringify(
            target
          )}(${Parameters.map(
            Parameter =>
              Parameter +
              `(${
                typeof Tokens[i] === 'undefined'
                  ? '?'
                  : stringify(Injections[i])
              })`
          ).toString()})!`
        );
      }

      const tokenDependency = this.global.getDependency(token);

      if (typeof tokenDependency === 'undefined') {
        throw new Error(
          `${chalk.redBright(
            chalk.bold('[ERROR]')
          )} There's no dependency for ${stringify(token)}!`
        );
      }

      let tokenInstance = this.global.getInstance(token);

      if (typeof tokenInstance === 'undefined') {
        if (typeof tokenDependency === 'function') {
          tokenInstance = this.resolve(
            tokenDependency,
            this.global.getDependencyContainerKey(tokenDependency)
          );
        } else if (typeof tokenDependency === 'object') {
          tokenInstance = this.resolveWithInjections(
            (<any>tokenDependency).__class__,
            (<any>tokenDependency).injections,
            this.global.getDependencyContainerKey(tokenDependency)
          );
        }
      }

      return tokenInstance;
    });

    return Injections;
  }

  /**
   * @returns The instance from the class that is the target
   * @param target The class to resolve instance from
   * @param key The key to add the instance and/or the dependency with
   */
  public resolve<T>(
    target: Type<T>,
    containerKey?: any,
    allowAccessTo?: boolean,
    key: Type<any> = target
  ): T {
    const classInstance = this.global.getInstance(target, false);
    if (classInstance) {
      return classInstance;
    }

    const injections = this.resolveInjections(target, containerKey);

    const newClassInstance = new target(...injections);

    if (typeof containerKey === 'undefined') {
      if (typeof this.global.getDependency(target, false) === 'undefined')
        this.global.setDependency(key, target, allowAccessTo);
      this.global.setInstance(key, newClassInstance, allowAccessTo);
    } else {
      if (
        typeof this.global
          .getDIContainer(containerKey)
          .getDependency(target, false) === 'undefined'
      )
        this.global.getDIContainer(containerKey).setDependency(key, target, allowAccessTo);
      this.global
        .getDIContainer(containerKey)
        .setInstance(key, newClassInstance, allowAccessTo);
    }

    return newClassInstance;
  }

  /**
   * @returns The instance from the class that is the target
   * @param target The class to resolve instance from
   * @param injections The specific dependencies instances that are available to inject into the constructor
   * @param key The key to add the instance and/or the dependency with
   */
  public resolveWithInjections<T>(
    target: Type<T>,
    injections: any[],
    containerKey?: any,
    allowAccessTo?: boolean,
    key: Type<any> = target
  ): T {
    const classInstance = this.global.getInstance(target, false);
    if (classInstance) {
      return classInstance;
    }

    const newClassInstance = new target(...injections);

    if (typeof containerKey === 'undefined') {
      if (typeof this.global.getDependency(target, false) === 'undefined')
        this.global.setDependency(key, target, allowAccessTo);
      this.global.setInstance(key, newClassInstance, allowAccessTo);
    } else {
      if (
        typeof this.global
          .getDIContainer(containerKey)
          .getDependency(target, false) === 'undefined'
      )
        this.global
          .getDIContainer(containerKey)
          .setDependency(key, target, allowAccessTo);
      this.global
        .getDIContainer(containerKey)
        .setInstance(key, newClassInstance, allowAccessTo);
    }

    return newClassInstance;
  }
}
