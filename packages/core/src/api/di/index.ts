/**
 * @license
 * 
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://gitlab.com/symetrical/symetrical/blob/master/LICENSE
 */


/**
 * @module
 * @description This module provides DI classes.
 */
export * from "./injector";
