/**
 * @license
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://gitlab.com/symetrical/symetrical/blob/master/LICENSE
 */

import chalk from 'chalk';

import {stringify} from '../util';

import {Type, Dependency} from '../interfaces';

/**
 * @description Defines every injection container in any injector.
 */
export class DIContainer {
  public father: DIContainer;
  public containers: DIContainer[];

  private allowAccessTo: boolean[];
  private instances: any[];
  private dependencies: Dependency[];

  constructor() {
    this.allowAccessTo = [];
    this.instances = [];
    this.dependencies = [];
    this.containers = [];
  }

  /**
   * @returns The dependency injection container by it's key
   * @param key The container key
   */
  public getDIContainer(key: any): DIContainer {
    if (typeof this.containers[key] !== 'undefined')
      return this.containers[key];
    else
      for (let i = 0; i < Object.keys(this.containers).length; i++) {
        const keys = Object.keys(this.containers);

        if (typeof this.containers[keys[i]].getDIContainer(key) !== 'undefined')
          return this.containers[keys[i]].getDIContainer(key);
      }
  }

  public hasContainer(key: any): boolean {
    return typeof this.getDIContainer(key) !== 'undefined';
  }

  public addDIContainer(key: any): void {
    this.containers[key] = new DIContainer();
    this.containers[key].father = this;
  }

  /**
   * @returns The container that contains the dependency inside
   * @param key The dependency key
   */
  public getDependencyContainer(key: any): DIContainer {
    if (this.hasDependency(key)) return this;

    const containerKeys = Object.keys(this.containers);

    for (let i = 0; i < containerKeys.length; i++)
      if (this.containers[containerKeys[i]].hasDependency(key))
        return this.containers[containerKeys[i]];
      else if (
        typeof this.containers[containerKeys[i]].getDependencyContainer(key) !==
        'undefined'
      )
        return this.containers[containerKeys[i]].getDependencyContainer(key);
  }

  public getDIContainerKey(di: DIContainer): any {
    const keys = Object.keys(this.containers);

    for (let i = 0; i < keys.length; i++)
      if (this.containers[keys[i]] == di) return keys[i];
    return undefined;
  }

  /**
   * @returns The key of the container that contains the dependency with the key inside of it
   * @param key The dependency key
   */
  public getDependencyContainerKey(key: any): any {
    if (this.hasDependency(key))
      return;

    const containerKeys = Object.keys(this.containers);

    for (let i = 0; i < containerKeys.length; i++)
      if (this.containers[containerKeys[i]].hasDependency(key))
        return containerKeys[i];
      else if (
        typeof this.containers[containerKeys[i]].getDependencyContainer(key) !==
        'undefined'
      )
        return this.containers[containerKeys[i]].getDependencyContainerKey(key);
  }

  public getInstanceContainerKey(key: any): any {
    if (this.hasInstance(key)) return;

    const containerKeys = Object.keys(this.containers);

    for (let i = 0; i < containerKeys.length; i++)
      if (this.containers[containerKeys[i]].hasInstance(key))
        return containerKeys[i];
  }

  /**
   * @description Gets a instance by it's key.
   * @param key The key to find the instance.
   */
  getInstance(
    key: any,
    limit: boolean = true,
    beggining: DIContainer = this
  ): any {
    const keys = Object.keys(this.containers);

    if (limit) {
      if (
        (this.hasInstance(key) &&
          (beggining == this || beggining == this.father)) ||
        this.allowAccessTo[key]
      )
        return this.instances[key];
      else if (
        beggining != this &&
        beggining != this.father &&
        !this.allowAccessTo[key]
      )
        throw new Error(
          `${chalk.redBright(
            chalk.bold('[ERROR]')
          )} The instance of ${stringify(key)} is out of reach!`
        );
      else if (
        typeof this.father !== 'undefined' &&
        this.father.allowAccessTo[key] == true &&
        this.father != beggining
      )
        return this.father.getInstance(key, true, beggining);
      else
        for (let i = 0; i < keys.length; i++)
          if (this.containers[keys[i]] != beggining)
            if (
              typeof this.containers[keys[i]].getInstance(
                key,
                true,
                beggining
              ) !== 'undefined'
            )
              return this.containers[keys[i]].getInstance(key, true, beggining);
    } else {
      if (this.hasInstance(key)) return this.instances[key];
      else if (typeof this.father !== 'undefined' && this.father != beggining)
        return this.father.getInstance(key, false, beggining);
      else {
        for (let i = 0; i < keys.length; i++)
          if (this.containers[keys[i]] != beggining)
            if (
              typeof this.containers[keys[i]].getInstance(
                key,
                false,
                beggining
              ) !== 'undefined'
            )
              return this.containers[keys[i]].getInstance(
                key,
                false,
                beggining
              );
      }
    }

    return undefined;
  }

  /**
   * @description Gets a dependency by it's key.
   * @param key The key to find the dependency.
   */
  public getDependency(
    key: any,
    limit: boolean = true,
    beggining: DIContainer = this
  ): Type<any> | {injections: any[]; __class__: Type<any>} {
    const keys = Object.keys(this.containers);

    if (limit) {
      if (
        this.hasDependency(key) &&
        (beggining == this ||
          (beggining == this.father && typeof this.father !== 'undefined') ||
          this.allowAccessTo[key])
      )
        return typeof this.dependencies[key] === 'function' ||
          typeof this.dependencies[key] === 'undefined'
          ? <any>this.dependencies[key]
          : <any>this.dependencies[key];
      else if (
        beggining != this &&
        beggining != this.father &&
        !this.allowAccessTo[key]
      )
        throw new Error(
          `${chalk.redBright(
            chalk.bold('[ERROR]')
          )} The dependency of ${stringify(key)} is out of reach!`
        );
      else if (
        typeof this.father !== 'undefined' &&
        this.father.allowAccessTo[key] == true &&
        this.father != beggining
      )
        return this.father.getDependency(key, limit, beggining);
      else
        for (let i = 0; i < keys.length; i++)
          if (this.containers[keys[i]] != beggining)
            if (
              typeof this.containers[keys[i]].getDependency(
                key,
                true,
                beggining
              ) !== 'undefined'
            )
              return this.containers[keys[i]].getDependency(
                key,
                true,
                beggining
              );
    } else {
      if (this.hasDependency(key))
        return typeof this.dependencies[key] === 'function' ||
          typeof this.dependencies[key] === 'undefined'
          ? <any>this.dependencies[key]
          : <any>this.dependencies[key];
      else if (typeof this.father !== 'undefined' && this.father != beggining)
        return this.father.getDependency(key, false, beggining);
      else
        for (let i = 0; i < keys.length; i++)
          if (this.containers[keys[i]] != beggining)
            if (
              typeof this.containers[keys[i]].getDependency(
                key,
                false,
                beggining
              ) !== 'undefined'
            )
              return this.containers[keys[i]].getDependency(
                key,
                false,
                beggining
              );
    }

    return undefined;
  }

  /**
   * @returns If a dependency exists inside of this container.
   * @param key The dependency key
   */
  public hasDependency(key: any, containerKey?: any): boolean {
    if (typeof containerKey != 'undefined')
      return this.containers[containerKey].hasDependency(key);

    return typeof this.dependencies[key] !== 'undefined';
  }

  /**
   * @returns If a instance exists inside of this container
   * @param key The instance key
   */
  public hasInstance(key: any, containerKey?: any): boolean {
    if (typeof containerKey != 'undefined')
      return this.containers[containerKey].hasInstance(key);

    return typeof this.instances[key] !== 'undefined';
  }

  /**
   * @description Set's a instance to something.
   * @param key The key to find the instance.
   * @param value The value to set the instance to.
   */
  public setInstance(
    key: any,
    value: any,
    allowAccessTo: boolean = true,
    containerKey?: any
  ): void {
    const old = this.instances[key];

    if (typeof containerKey != 'undefined')
      this.containers[containerKey].setInstance(key, value);
    else this.instances[key] = value;

    if (typeof old === 'undefined') this.allowAccessTo[key] = allowAccessTo;
  }

  /**
   * @description Set's a dependency of something.
   * @param key The key to find the dependency to set.
   * @param value The value to set the dependency to.
   */
  public setDependency(
    key: any,
    value: Type<any> | {injections: any[]; __class__: Type<any>},
    allowAccessTo: boolean = true,
    containerKey?: any
  ): void {
    const old = this.dependencies[key];

    if (typeof containerKey != 'undefined')
      this.containers[containerKey].setDependency(key, value);
    else this.dependencies[key] = value;

    if (typeof old === 'undefined') this.allowAccessTo[key] = allowAccessTo;
  }
}
