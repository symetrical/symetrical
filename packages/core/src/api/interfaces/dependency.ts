import {Type} from './type';

export type Dependency = Type<any> | {injections: any[]; __class__: Type<any>};
