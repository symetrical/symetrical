/**
 * @license
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://gitlab.com/symetrical/symetrical/blob/master/LICENSE
 */

import { Injectable } from '../decorators/injectable';
import { IRoute } from '../decorators/route';

/**
 * @description The Router provides access to all the API routes.
 */
@Injectable()
export class Router {
  constructor(
    private routes: { metadata: IRoute; instance: any; }[]
  ) {}

  getAllRoutes() {
    return this.routes;
  }
}
