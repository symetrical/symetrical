/**
 * @license
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://gitlab.com/symetrical/symetrical/blob/master/LICENSE
 */

import * as restify from "restify";

import { Injectable } from "../decorators/injectable";

/**
 * @description The server provider that is provided as default for every RESTfull API.
 */
@Injectable()
export class Server {
  constructor(public application: restify.Server) {}
}
