/**
 * @license
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://gitlab.com/symetrical/symetrical/blob/master/LICENSE
 */

import * as restify from 'restify';

import {camelize} from '../api/util';

/**
 * @description Defines the metadata required to create a new route using the decorator.
 */
export interface IRoute {
  method: 'GET' | 'POST' | 'DELETE' | 'OPTIONS' | 'PATCH' | 'PUT';
  containerKey?: any;
  path?: string;
  middlewares?: restify.RequestHandler[];
}

/**
 * @description Defines a method as a route.
 * @param metadata Required metadata to create a new route using the decorator.
 */
export function Route(metadata: IRoute): MethodDecorator {
  return (target, propertyKey: string, descriptor: PropertyDescriptor) => {
    if (typeof metadata.middlewares === 'undefined') metadata.middlewares = [];
    if (typeof metadata.path === 'undefined')
      metadata.path = '/' + camelize(propertyKey);

    if (!target.constructor['rmCallbacks'])
      target.constructor['rmCallbacks'] = [];

    target.constructor['rmCallbacks'].push(target => {
      metadata.path = target['rm'].path + metadata.path;

      target['rm'].routes.push({
        methodName: propertyKey,
        metadata
      });
    });
  };
}
