/**
 * @license
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://gitlab.com/symetrical/symetrical/blob/master/LICENSE
 */

/**
 * @description Defines a class as a Resource. A Resource contains routes
 * that have a relative path to the Resource.
 * 
 * @param path The path of the controller
 */
export function Resource(path: string): ClassDecorator {
    return target => {
        target["rm"] = {
            path,
            routes: []
        };

        if (target["rmCallbacks"])
            target["rmCallbacks"].forEach(rmFunc => rmFunc(target));

        return target;
    };
}
