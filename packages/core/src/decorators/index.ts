/**
 * @license
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://gitlab.com/symetrical/symetrical/blob/master/LICENSE
 */

/**
 * @module
 * @description Provides every decorator that you need to use the framework.
 */
export * from './bootstrap';
export * from './injectable';
export * from './module';
export * from './resource';
export * from './route';
