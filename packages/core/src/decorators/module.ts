/**
 * @license
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://gitlab.com/symetrical/symetrical/blob/master/LICENSE
 */

import * as restify from 'restify';

import {Type} from '../api';

import {AdvancedProvider} from '../interfaces';

/**
 * @description Defines the metadata required to use the decorator of every module.
 */
export interface IModule {
  containerKey?: any;
  providers?: (Type<any> | AdvancedProvider)[];
  imports?: (any | Type<any>)[];
  uses?: restify.RequestHandlerType[];
  preflights?: restify.RequestHandlerType[];
  resources?: (any | Type<any>)[];
  exports?: any[];
}

/**
 * @description Defines a Symetrical Module using metadata.
 * @param metadata The required metadata to use this decorator.
 */
export function SMModule(metadata: IModule): ClassDecorator {
  return target => {
    if (typeof metadata.imports === 'undefined') metadata.imports = [];
    if (typeof metadata.providers === 'undefined') metadata.providers = [];
    if (typeof metadata.resources === 'undefined') metadata.resources = [];
    if (typeof metadata.uses === 'undefined') metadata.uses = [];
    if (typeof metadata.preflights === 'undefined') metadata.preflights = [];
    if (typeof metadata.exports === 'undefined') metadata.exports = [];

    metadata.imports.forEach(module => {
      const moduleMetadata: IModule = module['mm'];
      moduleMetadata.providers.forEach(provider => {
        if (!moduleMetadata.exports.includes(provider)) {
          provider['allowAccessTo'] = false;
          provider['containerKey'] = module;
        }
        metadata.providers.unshift(provider);
      });
      moduleMetadata.imports.forEach(import_ => {
        if (!moduleMetadata.exports.includes(import_)) {
          import_['allowAccessTo'] = false;
          import_['containerKey'] = module;
        }
        metadata.imports.unshift(import_);
      });
      moduleMetadata.resources.forEach(resource => {
        resource['allowAccessTo'] = true;
        metadata.resources.unshift(resource);
      });
      moduleMetadata.uses.forEach(use => {
        metadata.uses.unshift(use);
      });
      moduleMetadata.preflights.forEach(pre => {
        metadata.preflights.unshift(pre);
      });
    });

    target['mm'] = metadata;

    return target;
  };
}
