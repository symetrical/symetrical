/**
 * @license
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://gitlab.com/symetrical/symetrical/blob/master/LICENSE
 */

import * as restify from 'restify';
import chalk from 'chalk';

import {stringify} from '../api/util';
import {Injector} from '../api/di';

import {Type} from '../api';

import {Server, Router} from '../providers';

import {IRoute} from './route';
import {IModule} from './module';
import {AdvancedProvider, Request, Response, Next} from '../interfaces';

/**
 * @description The metadata required to create the Starting Point for a RESTfull API.
 */
export class IBootstrap {
  uses?: restify.RequestHandlerType[];
  preflights?: restify.RequestHandlerType[];
  restifyServerOptions?: restify.ServerOptions;
  imports?: any[] | Type<any>[];
  providers?: (Type<any> | AdvancedProvider)[];
  resources?: any[] | Type<any>[];
  port?: number;
}

/**
 * @description Defines the Starting Point of a RESTfull API.
 * @param metadata The metadata required to use the decorator.
 */
export function Bootstrap(metadata: IBootstrap): ClassDecorator {
  return target => {
    if (typeof metadata.imports === 'undefined') metadata.imports = [];
    if (typeof metadata.port === 'undefined') metadata.port = 3000;
    if (typeof metadata.providers === 'undefined') metadata.providers = [];
    if (typeof metadata.resources === 'undefined') metadata.resources = [];
    if (typeof metadata.uses === 'undefined') metadata.uses = [];
    if (typeof metadata.preflights === 'undefined') metadata.preflights = [];

    const injector = new Injector();

    for (let i = 0; i < metadata.imports.length; i++) {
      const Import = metadata.imports[i];
      const ModuleMetadata: IModule = Import['mm'];
      injector.global.addDIContainer(Import);

      if (typeof ModuleMetadata === 'undefined') {
        throw new Error(
          `${chalk.redBright(chalk.bold('[ERROR]'))} ${stringify(
            Import
          )} is not a Symetrical Module, check if you added the @SMModule decorator into it!`
        );
      }

      for (let i = 0; i < ModuleMetadata.imports.length; i++) {
        const Module = ModuleMetadata.imports[i];
        if (typeof Module['containerKey'] === 'undefined')
          Module['containerKey'] = Import;
        injector.global.containers[Import].addDIContainer(Module);
      }

      for (let i = 0; i < ModuleMetadata.resources.length; i++) {
        const Resource = ModuleMetadata.resources[i];
        if (typeof Resource['containerKey'] === 'undefined')
          Resource['containerKey'] = Import;
        metadata.resources.push(Resource);
      }

      for (let i = 0; i < ModuleMetadata.providers.length; i++) {
        const Provider: any = ModuleMetadata.providers[i];
        if (typeof Provider['containerKey'] === 'undefined')
          Provider['containerKey'] = Import;
        metadata.providers.push(Provider);
      }

      for (let i = 0; i < ModuleMetadata.uses.length; i++) {
        const Use = ModuleMetadata.uses[i];

        metadata.uses.push(Use);
      }

      for (let i = 0; i < ModuleMetadata.preflights.length; i++) {
        const Preflight = ModuleMetadata.preflights[i];

        metadata.preflights.push(Preflight);
      }
    }

    metadata.providers.forEach((provider, i) => {
      metadata.providers[i]['allowAccessTo'] =
        typeof provider['allowAccessTo'] === 'undefined'
          ? true
          : provider['allowAccessTo'];
    });

    metadata.imports.forEach((module, i) => {
      metadata.imports[i]['allowAccessTo'] =
        typeof module['allowAccessTo'] === 'undefined'
          ? true
          : module['allowAccessTo'];
    });

    metadata.resources.forEach((resource, i) => {
      metadata.resources[i]['allowAccessTo'] =
        typeof resource['allowAccessTo'] === 'undefined'
          ? true
          : resource['allowAccessTo'];
    });

    injector.resolveWithInjections(
      Server,
      [restify.createServer(metadata.restifyServerOptions)],
      undefined,
      true
    );

    injector.global.setDependency(IBootstrap, IBootstrap, true);

    metadata.uses.forEach(use =>
      injector.global.getInstance(Server).application.use(use)
    );
    metadata.preflights.forEach(pre =>
      injector.global.getInstance(Server).application.pre(pre)
    );

    const routes: {
      metadata: IRoute;
      methodName: string;
      instance: any;
    }[] = [];

    injector.global.setDependency(Router, Router, true);

    for (let i = 0; i < metadata.resources.length; i++) {
      if (typeof metadata.resources[i]['rm'] === 'undefined') {
        throw new Error(
          `${chalk.redBright(chalk.bold('[ERROR]'))} ${stringify(
            metadata.resources[i]
          )} is not a resource! Check out if you added the @Resource decorator to it's class!`
        );
      }

      injector.global.setDependency(
        metadata.resources[i],
        metadata.resources[i],
        metadata.resources[i].allowAccessTo,
        metadata.resources[i].containerKey
      );
    }

    injector.global.setInstance(IBootstrap, metadata);

    for (let i = 0; i < metadata.imports.length; i++) {
      injector.global.setDependency(
        metadata.imports[i],
        metadata.imports[i],
        metadata.imports[i].allowAccessTo,
        metadata.imports[i].containerKey
      );
    }

    for (let i = 0; i < metadata.providers.length; i++) {
      const Provider = metadata.providers[i];

      if (typeof Provider === 'function') {
        injector.global.setDependency(
          Provider,
          Provider,
          Provider['allowAccessTo'],
          Provider['containerKey']
        );
      } else {
        injector.global.setDependency(
          (<AdvancedProvider>Provider).key,
          (<AdvancedProvider>Provider).dependency,
          (<AdvancedProvider>Provider).allowAccessTo,
          (<AdvancedProvider>Provider).containerKey
        );
      }
    }

    for (let i = 0; i < metadata.resources.length; i++) {
      const resource = metadata.resources[i];

      let instance = injector.global.getInstance(resource, false);

      if (typeof instance === 'undefined')
        instance = injector.resolve(
          resource,
          undefined,
          resource.allowAccessTo
        );

      const RM = resource['rm'];

      for (let i = 0; i < RM.routes.length; i++) {
        routes.push({
          metadata: RM.routes[i].metadata,
          methodName: RM.routes[i].methodName,
          instance
        });
      }
    }

    injector.resolveWithInjections(Router, [routes]);

    for (let i = 0; i < metadata.imports.length; i++) {
      injector.resolve(
        metadata.imports[i],
        metadata.imports[i].containerKey,
        metadata.imports[i].allowAccessTo
      );
    }

    for (let i = 0; i < metadata.providers.length; i++) {
      const Provider = metadata.providers[i];

      if (typeof Provider === 'function') {
        injector.resolve(
          <Type<any>>Provider,
          (<any>Provider).containerKey,
          (<any>Provider).allowAccessTo
        );
      } else {
        injector.resolve(
          (<AdvancedProvider>Provider).dependency,
          (<AdvancedProvider>Provider).key,
          (<AdvancedProvider>Provider).allowAccessTo,
          (<AdvancedProvider>Provider).containerKey
        );
      }
    }

    injector.resolve(<any>target);
    routes.forEach(Route => {
      if (Route.metadata.method === 'GET') {
        injector.global
          .getInstance(Server)
          .application.get(
            Route.metadata.path,
            (req: Request, res: Response, next: Next) => {
              let i = 0;
              while (i < Route.metadata.middlewares.length) {
                const Middleware: any = Route.metadata.middlewares[i];

                Middleware(req, res, () => i++);
              }

              if (!res.headersSent)
                Route.instance[Route.methodName](req, res, next);
            }
          );
      }

      if (Route.metadata.method === 'POST') {
        injector.global
          .getInstance(Server)
          .application.post(
            Route.metadata.path,
            (req: Request, res: Response, next: Next) => {
              let i = 0;
              while (i < Route.metadata.middlewares.length) {
                const Middleware: any = Route.metadata.middlewares[i];

                Middleware(req, res, () => i++);
              }

              if (!res.headersSent)
                Route.instance[Route.methodName](req, res, next);
            }
          );
      }

      if (Route.metadata.method === 'PUT') {
        injector.global
          .getInstance(Server)
          .application.put(
            Route.metadata.path,
            (req: Request, res: Response, next: Next) => {
              let i = 0;
              while (i < Route.metadata.middlewares.length) {
                const Middleware: any = Route.metadata.middlewares[i];

                Middleware(req, res, () => i++);
              }

              if (!res.headersSent)
                Route.instance[Route.methodName](req, res, next);
            }
          );
      }

      if (Route.metadata.method === 'DELETE') {
        injector.global
          .getInstance(Server)
          .application.del(
            Route.metadata.path,
            (req: Request, res: Response, next: Next) => {
              let i = 0;
              while (i < Route.metadata.middlewares.length) {
                const Middleware: any = Route.metadata.middlewares[i];

                Middleware(req, res, () => i++);
              }

              if (!res.headersSent)
                Route.instance[Route.methodName](req, res, next);
            }
          );
      }

      if (Route.metadata.method === 'PUT') {
        injector.global
          .getInstance(Server)
          .application.patch(
            Route.metadata.path,
            (req: Request, res: Response, next: Next) => {
              let i = 0;
              while (i < Route.metadata.middlewares.length) {
                const Middleware: any = Route.metadata.middlewares[i];

                Middleware(req, res, () => i++);
              }

              if (!res.headersSent)
                Route.instance[Route.methodName](req, res, next);
            }
          );
      }

      if (Route.metadata.method === 'OPTIONS') {
        injector.global
          .getInstance(Server)
          .application.opts(
            Route.metadata.path,
            (req: Request, res: Response, next: Next) => {
              let i = 0;
              while (i < Route.metadata.middlewares.length) {
                const Middleware: any = Route.metadata.middlewares[i];

                Middleware(req, res, () => i++);
              }

              if (!res.headersSent)
                Route.instance[Route.methodName](req, res, next);
            }
          );
      }
    });

    injector.global
      .getInstance(Server)
      .application.listen(metadata.port, () => {
        console.log(
          `${chalk.magentaBright(chalk.bold('[LOG]'))} Listening on the port ${
            metadata.port
          }!`
        );
      });
  };
}
