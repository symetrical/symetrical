/**
 * @license
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://gitlab.com/symetrical/symetrical/blob/master/LICENSE
 */

/**
 * @description Required to define that a provider is injectable. More detailably
 * it let's you get the parameters from the class constructor.
 */
export function Injectable(): ClassDecorator {
  return target => target;
}
