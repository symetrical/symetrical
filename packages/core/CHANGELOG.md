# Symetrical core change log

## 2.9.0

- Now a resource will always be exported.

## 2.8.3

- I may have been dumb, but now it should work. || Eu posso ter sido meio burro, mas agora vai funcionar.

## 2.8.2

- Tive que resolver um outro problema no meu código || Had to solve another problem.

## 2.8.1

- Reosolvi um pequeno problema dentro da API de injeção de dependências.

## 2.8.0

- Agora as injeções de dependências entre módulos é diferente, anteriormente você poderia criar um módulo, e todas as dependências dele seria exportada para os módulos que o importarem, mas agora as coisas que você exporta são opcionais.

## 2.7.2

- Resolvi uma est�pidez que fiz.

## 2.7.0

- Changed the Route decorator to be made for methods.
- Now to create a route, you need to create a Resource.
- In the Route decorator metadata you can give it middlewares.
- The Resource has a path, and every route inside of it is a method that has a relative path to it's resource.

## 2.6.3

- Added a dependency for the metadata inside the Bootstrap StartingPoint.

## 2.6.21

- Actually fixed the problem with ther Router.

## 2.6.2

- Fixed the problem with the Router, when you added it as a provider inside a module, and then imported it in the StartingPoint of the API.

## 2.6.1

- Fixed the package.json main file little problem.

## 2.6.0

- Added a Router that provides access to every route in the API.
- Changed the "outDir" to "dist", inside the tsconfig.json file.

## 2.5.1

- Fixed the error that happend when the StartingPoint loaded a normal provider.

## 2.5.0

- Added the "AdvancedProvider" feature. (A "advanced provider" is actually just a specification of the token that the injector should search for inside a constructor's parameters and when it finds that token, it injects the specified dependecy. So you can do things like customizing the framework things, like routing, or some other things that will appear on the future)

## 2.4.1

- Fixed the problem that happend when you added a child route into a child that has parameters. (That's a very weird case)

## 2.4.0

- Now it's necessary to add every dependency for the DI to work.

## 2.3.2

- Changed the dist, to actually be the root of the package.

## 2.3.1

- Fixed the missing export in the first "index.ts".

## 2.3.0

- Fixed a bunch of errors.
- Added some comments here and there.
- Now, a class can resolve it's own dependencies, so some dependencies you don't need to add somewhere.
- Now the Injector is Reflective, so it doesn't need to use the old technique.

## 2.2.11

- Fixed the repository url in the README for the npm again.

## 2.2.1

- Fixed the repository url in the README for the npm.

## 2.2.0

- Changed the hole base to Restify.
- Added a interface to define a Request, a Response, and the NextFunction.

## 2.1.01

- Changed the README again.

## 2.1.0

- Changed the README.

## 2.0.91

- Fixed a little problem inside the Injector.
