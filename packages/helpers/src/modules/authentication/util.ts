/**
 * @license
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://gitlab.com/symetrical/symetrical/blob/master/LICENSE
 */
import { Injectable } from '@symetrical/core';

import { TypeEnum } from './type.enum';

@Injectable()
export class Util {
  constructor() {}

  fromTypeToTypeEnum(type: string): TypeEnum {
    if (type == 'number')
      return TypeEnum.NUMBER;
    else if (type == 'string')
      return TypeEnum.STRING;
    else if (type == 'boolean')
      return TypeEnum.BOOLEAN;
  }

  isOfType(obj: object, type_: { [name: string]: TypeEnum }): boolean {
    const keys: string[] = Object.keys(obj);
    const expectedKeys: string[] = Object.keys(type_);
    
    // Tests if every key of type_ is inside of obj
    if (keys != expectedKeys) return false;

    // Tests if every property of obj is of the respective type with type_
    const types: TypeEnum[] = keys.map(key => this.fromTypeToTypeEnum(typeof obj[key]));
    const expectedTypes: TypeEnum[] = expectedKeys.map(key => type_[key]);

    if (types != expectedTypes) return false;

    return true;
  }
}
