/**
 * @license
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://gitlab.com/symetrical/symetrical/blob/master/LICENSE
 */
import { TypeEnum } from "./type.enum";

/**
 * @description This is the Authentication Options class which defines some configurations for the authentication module.
 */
export class AuthenticationOptions {
	tokenExpires: boolean;
	tokenExpirationOptions: {
		expiresIn: number; // Milisseconds
	};
  jwtSecretOrPrivateKey: string;
  passType: { [name: string]: TypeEnum };

	constructor(
    jwtSecretOrPrivateKey: string,
    passType: { [name: string]: TypeEnum },
		tokenExpires: boolean = false,
		tokenExpirationOptions?: { expiresIn: number }
	) {
    this.tokenExpires = tokenExpires;
    this.tokenExpirationOptions = tokenExpirationOptions;
    this.jwtSecretOrPrivateKey = jwtSecretOrPrivateKey;
    this.passType = passType;
  }
}
