/**
 * @license
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://gitlab.com/symetrical/symetrical/blob/master/LICENSE
 */
import { SMModule } from '@symetrical/core';
import { Type } from '@symetrical/core/lib/api';

import { AuthenticationResource } from './authentication.resource';

import { AuthController } from './auth.controller';
import { Util } from './util';

import { AuthenticationOptions } from './authentication.options';

/**
 * @description Don't forget to add the bodyParser and the queryParser middleware to the "uses" in the Starting Point's Bootstrap decorator.
 */
@SMModule({
  providers: [AuthController, Util],
  resources: [AuthenticationResource],
  exports: [AuthController]
})
export class AuthenticationModule {
  static AuthenticationOptions: AuthenticationOptions;

  static Initialize(config: AuthenticationOptions): Type<AuthenticationModule> {
    this.AuthenticationOptions = config;
    this['mm'].providers = [AuthController, Util];
    this['mm'].resources = [AuthenticationResource];
    this['mm'].exports = [AuthController];
    return AuthenticationModule;
  }
}
