/**
 * @license
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://gitlab.com/symetrical/symetrical/blob/master/LICENSE
 */
import { Resource, Route, Request, Response, Next } from '@symetrical/core';

import { AuthController } from './auth.controller';
import { AuthenticationModule } from './authentication.module';
import { Util } from './util';

@Resource('/')
export class AuthenticationResource {
  constructor(private authController: AuthController, private util: Util) {}

  @Route({
    method: 'GET'
  })
  async verify(req: Request, res: Response, next: Next) {
    if (typeof req.query.token === undefined) {
      res.status(400);
      res.json({
        message: 'Missing the parameter "token" in the query.',
        code: 400
      });
      next();
    } else {
      res.status(200);
      res.json(await this.authController.verify(req.query.token));
      next();
    }
  }

  @Route({
    method: 'POST'
  })
  sign(req: Request, res: Response, next: Next) {
    if (this.util.isOfType(req.body, AuthenticationModule.AuthenticationOptions.passType)) {
      res.status(200);
      res.json({
        code: 200,
        token: this.authController.sign(req.body)
      });
      next();
    } else {
      res.status(400);
      res.json({
        code: 400,
        message: 'It seems that the request body isn\'t the of the type it should be.'
      });
      next();
    }
  }
}
