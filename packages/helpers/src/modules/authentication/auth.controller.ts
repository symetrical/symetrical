/**
 * @license
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://gitlab.com/symetrical/symetrical/blob/master/LICENSE
 */
import { Injectable } from '@symetrical/core';

import * as jwt from 'jsonwebtoken';

import { AuthenticationModule } from './authentication.module';

@Injectable()
export class AuthController {
	constructor() {}

	sign(pass: any): Promise<string> {
		return new Promise<string>((resolve, reject) => {
			if (AuthenticationModule.AuthenticationOptions.tokenExpires)
				jwt.sign(
					pass,
					AuthenticationModule.AuthenticationOptions.jwtSecretOrPrivateKey,
					{
						expiresIn: AuthenticationModule.AuthenticationOptions.tokenExpirationOptions.expiresIn
					},
					(err, token) => {
						if (err) return reject(err);

						resolve(token);
					}
				);
			else
				jwt.sign(
					pass,
					AuthenticationModule.AuthenticationOptions.jwtSecretOrPrivateKey,
					(err: Error, token: string) => {
						if (err) return reject(err);

						resolve(token);
					}
				);
		});
	}

	verify(token: string): Promise<object> {
		return new Promise<object>((resolve, reject) => {
			jwt.verify(token, AuthenticationModule.AuthenticationOptions.jwtSecretOrPrivateKey, (err, decoded) => {
				if (err) return reject(err);

				resolve(decoded);
			});
		});
	}
}
