/**
 * @license
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://gitlab.com/symetrical/symetrical/blob/master/LICENSE
 */

import { SMModule } from "@symetrical/core";
import { Type } from "@symetrical/core/lib/api";

import * as cors from "restify-cors-middleware";

/**
 * @description The module that provides CORS as use and preflight.
 */
@SMModule({})
export class CorsModule {
  public static Initialize(CorsOptions: cors.Options): Type<CorsModule> {
    const CorsMiddleware = cors(CorsOptions);

    (<any>CorsModule).mm.uses = [CorsMiddleware.actual];
    (<any>CorsModule).mm.preflights = [CorsMiddleware.preflight];

    return CorsModule;
  }
}
