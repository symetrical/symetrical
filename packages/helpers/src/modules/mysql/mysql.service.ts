/**
 * @license
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://gitlab.com/symetrical/symetrical/blob/master/LICENSE
 */

import { Injectable } from "@symetrical/core";

import * as mysql from "mysql";

import { MySqlModule } from "./mysql.module";

/**
 * @description The MySql service provider.
 */
@Injectable()
export class MySqlService {
  private Connection: mysql.Connection;

  constructor() {}

  private disconnect() {
    this.Connection.destroy();
    delete this.Connection;
  }

  private connect() {
    if (typeof this.Connection !== "undefined") this.disconnect();
    this.Connection = mysql.createConnection(MySqlModule.MySqlConfig);
    this.Connection.connect();
  }

  /**
   * @description Runs a query in the current MySql database.
   * @param query The query to run in the current MySql database.
   */
  query(query: string | mysql.Query): Promise<any> {
    return new Promise((resolve, reject) => {
      this.connect();
      this.Connection.query(query, (err, results) => {
        if (err) reject(err);

        this.disconnect();
        resolve(results);
      });
    });
  }
}
