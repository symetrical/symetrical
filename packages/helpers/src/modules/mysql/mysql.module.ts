/**
 * @license
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://gitlab.com/symetrical/symetrical/blob/master/LICENSE
 */

import { SMModule } from "@symetrical/core";
import { Type } from "@symetrical/core/lib/api";

import * as mysql from "mysql";

import { MySqlService } from "./mysql.service";

/**
 * @description The MySql module that provides the MySql service.
 */
@SMModule({
  providers: [MySqlService],
  exports: [MySqlService]
})
export class MySqlModule {
  static MySqlConfig: mysql.ConnectionConfig;

  /**
   * @description Initializes MySql.
   * @param config The config to initialize MySql with.
   */
  static Initialize(config: mysql.ConnectionConfig): Type<MySqlModule> {
    this.MySqlConfig = config;

    return MySqlModule;
  }
}
