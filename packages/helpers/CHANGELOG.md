# Symetrical helpers change log

## 2.4.0

- Added the authentication module that adds a "sign" and a "veryify" resource to sign in with and verify a token with.

## 2.3.1

- Fixed the error with "'<any>CorsModule.module_metadata.uses = ...' cannot set property of undefined...".

## 2.3.0

- Changed to match with the latest @symetrical/core version.

## 2.2.0

- Changed from dist to lib.

## 2.1.8

- Updated all the packages.

## 2.1.6

- Fixed some problems with new version of the core.

## 2.1.5

- Changed the core version to match with the latest version.

## 2.1.42

- Forgot to compile the new typescript code ;-;

## 2.1.41

- Fixed the import error inside the mysql module, and inside the cors module.

## 2.1.4

- Changed the Symetrical Core version inside the module to the newer version.
- Added the license warning everywhere.
- Changed the dist, to actually be the root of the package.

## 2.1.31

- Fixed the repository url in the README for the npm again.

## 2.1.3

- Fixed the repository url in the README for the npm.

## 2.1.2

- Changed the CorsModule to match with the new restify update in the @symetrical/core.
