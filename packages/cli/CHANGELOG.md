# @symetrical/cli

## 3.7.4

- Solved the problem in the generate command that would create files with tabs in every line.
- Solved the problem in the generate comamnd that would break the Starting Point metadata.

## 3.7.3

- Solved some problems related to the genarting command and the creation of a new project | Resolvi alguns problemas relacionados à geração de comandos e a criação de um novo projeto
- Added a test of compartion between the recommended version of the CLI of the project and the version of the CLI globally, now it warns you if both are different. | Adicionei o teste de comparação entre a versão do CLI recomendada do projeto e a versão do CLI globalmente, agora ele dá uma warning se a versão do CLI for diferente.

## 3.7.0

- Updated the version. || Atualizei as versões.

## 3.6.0

- Atualizei as versões dentro do Starter Kit.

## 3.5.1

- Havia esquecido de modificar a versão dentro do cli.json também. || I forgot to update the version inside the cli.json.

## 3.5.0

- Atualizei a versão do @symetrical/core dentro do Starter Kit.

## 3.4.3

- Fixed something inside the Starter Kit

## 3.4.2

- Compiled the Typescript.

## 3.4.0

- Updated to the latest version of the @symetrical/core.
- Added some stuff inside the cli.json that will make a difference in future versions.
- Deleted some useless files.

## 3.3.3

- Solved the problem that when you create a new project, instead of ".gitignore" it written ".npmignore".

## 3.3.2

- There was a problem on the compilation, and i didn't notice it in a long time ;-;

## 3.3.1

- Changed the schematic inside the generate command; now the 'route' is the 'resource'.

## 3.3.0

- Updated the Starter Kit code to work with the latest version of the core.
- Updated all the packages inside the CLI as well.

### 3.2.2

- Fixed the problem that when you generated a new file, the import added to Starting Point refering to that specific file generated was wrong most of the time.

### 3.2.1

- Fixed the bug when trying to create a new API on the moment of running the "git init" or "npm install" inside the new project.

### 3.2.0

- Fixed the bug inside the generate command, that when you generated any file, the import inside the Starting Point of the API ended with ".ts".
- Fixed the bug from the generate command, that when you generated any file, it added inside only the providers not respectively with the file schematic.
- When a file with the schematic "route" is generated, now, it's like "my.route.ts" not just "my.ts".
- When a file with the schematic "module" is generated, now, it's like "my.module.ts" not just "my.ts".

### 3.1.2

- Fixed the problem that the new command never ended.

### 3.1.1

- When you generate a file it's generated relativily to the Starting Point file.

### 3.1.0

- Added the generate command. With it you can generate routes, providers, modules.

### 3.0.1

- Fixed the "sm new <PROJECTNAME>" error.

### 3.0.0

- Written the CLI again now with the "oclif" CLI framework.

### 2.1.9

- Changed the Starter Kit @symetrical/core version to the latest version.

### 2.1.8

- Fixed the problem that when you created a new project with "sm new", he whould not create the
  ".gitignore" file.

### 2.1.7

- Changed the Starter Kit @symetrical/core version to the latest version.

### 2.1.6

- Changed the Starter Kit @symetrical/core version to the latest version.

### 2.1.5

- Fixed the "node_modules" name inside the ".gitignore".

### 2.1.4

- Changed the Starter Kit version to match with the Core latest version.

### 2.1.3

- Changed the Starter Kit @symetrical/core version to match with the latest version.

### 2.1.2

- Now the new command is more organized, for collaborators, easier to understand.

### 2.0.2

- Fixed a problem inside the new command.

### 2.0.1

- Fixed the "Cannot Find Module 'minimist'" error.

### 2.0.0

- Now works with the newer version of the @symetrical/core.
- Now the CLI works a little bit smoother.

### 1.8.41

- Fixed the repository url in the README for the npm again.

### 1.8.4

- Fixed the repository url in the README for the npm.
