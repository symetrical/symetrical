@symetrical/cli
===============

The Symetrical Framework CLI

[![oclif](https://img.shields.io/badge/cli-oclif-brightgreen.svg)](https://oclif.io)
[![Version](https://img.shields.io/npm/v/@symetrical/cli.svg)](https://npmjs.org/package/@symetrical/cli)
[![Downloads/week](https://img.shields.io/npm/dw/@symetrical/cli.svg)](https://npmjs.org/package/@symetrical/cli)
[![License](https://img.shields.io/npm/l/@symetrical/cli.svg)](https://github.com/tots/cli/blob/master/package.json)

<!-- toc -->
* [Usage](#usage)
* [Commands](#commands)
<!-- tocstop -->
* [Usage](#usage)
* [Commands](#commands)
<!-- tocstop -->
* [Usage](#usage)
* [Commands](#commands)
<!-- tocstop -->
# Usage
<!-- usage -->
```sh-session
$ npm install -g @symetrical/cli
$ sm COMMAND
running command...
$ sm (-v|--version|version)
@symetrical/cli/3.7.6 win32-x64 node-v12.16.1
$ sm --help [COMMAND]
USAGE
  $ sm COMMAND
...
```
<!-- usagestop -->
```sh-session
$ npm install -g @symetrical/cli
$ sm COMMAND
running command...
$ sm (-v|--version|version)
@symetrical/cli/3.7.3 win32-x64 node-v12.13.0
$ sm --help [COMMAND]
USAGE
  $ sm COMMAND
...
```
<!-- usagestop -->
```sh-session
$ npm install -g @symetrical/cli
$ sm COMMAND
running command...
$ sm (-v|--version|version)
@symetrical/cli/3.7.0 win32-x64 node-v8.15.0
$ sm --help [COMMAND]
USAGE
  $ sm COMMAND
...
```
<!-- usagestop -->
# Commands
<!-- commands -->
* [`sm generate SCHEMATIC FILENAME`](#sm-generate-schematic-filename)
* [`sm help [COMMAND]`](#sm-help-command)
* [`sm new PROJECTNAME`](#sm-new-projectname)

## `sm generate SCHEMATIC FILENAME`

generates a new thing to a Symetrical RESTfull API

```
USAGE
  $ sm generate SCHEMATIC FILENAME

ARGUMENTS
  SCHEMATIC  (resource|r|provider|p|module|m) the schematic type to write the file as
  FILENAME   where to write the file

OPTIONS
  -h, --help  show CLI help
```

_See code: [src\commands\generate.ts](https://gitlab.com/symetrical/symetrical-cli/blob/v3.7.6/src\commands\generate.ts)_

## `sm help [COMMAND]`

display help for sm

```
USAGE
  $ sm help [COMMAND]

ARGUMENTS
  COMMAND  command to show help for

OPTIONS
  --all  see all commands in CLI
```

_See code: [@oclif/plugin-help](https://github.com/oclif/plugin-help/blob/v2.1.6/src\commands\help.ts)_

## `sm new PROJECTNAME`

creates a new RESTfull API

```
USAGE
  $ sm new PROJECTNAME

ARGUMENTS
  PROJECTNAME  the name of the project to create

OPTIONS
  -h, --help  show CLI help
```

_See code: [src\commands\new.ts](https://gitlab.com/symetrical/symetrical-cli/blob/v3.7.6/src\commands\new.ts)_
<!-- commandsstop -->
* [`sm generate SCHEMATIC FILENAME`](#sm-generate-schematic-filename)
* [`sm help [COMMAND]`](#sm-help-command)
* [`sm new PROJECTNAME`](#sm-new-projectname)

## `sm generate SCHEMATIC FILENAME`

generates a new thing to a Symetrical RESTfull API

```
USAGE
  $ sm generate SCHEMATIC FILENAME

ARGUMENTS
  SCHEMATIC  (resource|r|provider|p|module|m) the schematic type to write the file as
  FILENAME   where to write the file

OPTIONS
  -h, --help  show CLI help
```

_See code: [src\commands\generate.ts](https://gitlab.com/symetrical/symetrical-cli/blob/v3.7.3/src\commands\generate.ts)_

## `sm help [COMMAND]`

display help for sm

```
USAGE
  $ sm help [COMMAND]

ARGUMENTS
  COMMAND  command to show help for

OPTIONS
  --all  see all commands in CLI
```

_See code: [@oclif/plugin-help](https://github.com/oclif/plugin-help/blob/v2.1.6/src\commands\help.ts)_

## `sm new PROJECTNAME`

creates a new RESTfull API

```
USAGE
  $ sm new PROJECTNAME

ARGUMENTS
  PROJECTNAME  the name of the project to create

OPTIONS
  -h, --help  show CLI help
```

_See code: [src\commands\new.ts](https://gitlab.com/symetrical/symetrical-cli/blob/v3.7.3/src\commands\new.ts)_
<!-- commandsstop -->
* [`sm generate SCHEMATIC FILENAME`](#sm-generate-schematic-filename)
* [`sm help [COMMAND]`](#sm-help-command)
* [`sm new PROJECTNAME`](#sm-new-projectname)

## `sm generate SCHEMATIC FILENAME`

generates a new thing to a Symetrical RESTfull API

```
USAGE
  $ sm generate SCHEMATIC FILENAME

ARGUMENTS
  SCHEMATIC  (resource|r|provider|p|module|m) the schematic type to write the file as
  FILENAME   where to write the file

OPTIONS
  -h, --help  show CLI help
```

_See code: [src\commands\generate.ts](https://gitlab.com/symetrical/symetrical-cli/blob/v3.7.0/src\commands\generate.ts)_

## `sm help [COMMAND]`

display help for sm

```
USAGE
  $ sm help [COMMAND]

ARGUMENTS
  COMMAND  command to show help for

OPTIONS
  --all  see all commands in CLI
```

_See code: [@oclif/plugin-help](https://github.com/oclif/plugin-help/blob/v2.1.6/src\commands\help.ts)_

## `sm new PROJECTNAME`

creates a new RESTfull API

```
USAGE
  $ sm new PROJECTNAME

ARGUMENTS
  PROJECTNAME  the name of the project to create

OPTIONS
  -h, --help  show CLI help
```

_See code: [src\commands\new.ts](https://gitlab.com/symetrical/symetrical-cli/blob/v3.7.0/src\commands\new.ts)_
<!-- commandsstop -->
