import {SMModule} from '@symetrical/core';

import {WordController} from '../providers/controllers/word.controller';

@SMModule({
  providers: [WordController],
  exports: [WordController]
})
export class ProvidersModule {}
