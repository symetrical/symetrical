import { Bootstrap } from '@symetrical/core';

import { ProvidersModule } from './modules/providers.module';

import { ExampleResource } from './resources/example.resource';

@Bootstrap({
  providers: [],
  resources: [ExampleResource],
  port: 3000,
  imports: [ProvidersModule],
  preflights: [],
  restifyServerOptions: {
    name: '$projectName$',
    version: '0.0.0'
  }
})
export class StartingPoint { }
