import {Injectable} from '@symetrical/core';

@Injectable()
export class WordController {
  private words: string[];

  constructor() {
    this.words = [];
  }

  getWords(): string[] {
    return this.words;
  }

  pushWord(word: string): string[] {
    this.words.push(word);

    return this.words;
  }
}
