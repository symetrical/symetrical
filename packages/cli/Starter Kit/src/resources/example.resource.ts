import { Resource, Route, Request, Response, Next } from '@symetrical/core';

import { WordController } from '../providers/controllers/word.controller';

@Resource('/example')
export class ExampleResource {
  constructor(private wordController: WordController) { }

  @Route({
    path: '',
    method: 'GET'
  })
  index(req: Request, res: Response, next: Next) {
    res.status(200);
    res.json(this.wordController.pushWord('Hey man! What did you expect to be in here?'));
    next();
  }

  @Route({
    path: '/:id',
    method: 'GET'
  })
  child(req: Request, res: Response, next: Next) {
    res.status(200);
    res.json(this.wordController.pushWord('Seriously what the hell did you expect to be in these texts?'));
    next();
  }
}
