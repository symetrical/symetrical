import {Command, flags} from '@oclif/command'
import * as path from 'path'

import * as FsHelper from '../helpers/fs'
import * as Log from '../helpers/log'

export default class New extends Command {
  static description = 'creates a new RESTfull API'

  static flags = {
    help: flags.help({char: 'h'})
  }

  static args = [
    {
      name: 'projectName',
      required: true,
      description: 'the name of the project to create'
    }
  ]

  async run() {
    const projectName: string = (this.parse(New).args as any).projectName

    if (FsHelper.Exists(projectName)) {
      this.log('Directory already exists!')
      this.exit()
    }

    await FsHelper.CreateDirectory(projectName)
    await FsHelper.CopyDirectory(
      `${path.dirname(path.dirname(__dirname))}/Starter Kit`,
      projectName
    )
    await FsHelper.RepalceInEveryFileOfDirectory(
      '$projectName$',
      projectName,
      projectName
    )

    this.log('\n')

    Log.RunCommand(`cd ${projectName} && git init`)
    Log.RunCommand(`cd ${projectName} && npm install`)
  }
}
