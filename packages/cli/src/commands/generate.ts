import {Command, flags} from '@oclif/command'
import * as path from 'path'

import * as Fs from '../helpers/fs'
import * as Logger from '../helpers/log'

type GenerationSchematic = 'resource' | 'r' | 'provider' | 'p' | 'module' | 'm'

export default class Generate extends Command {
  static description = 'generates a new thing to a Symetrical RESTfull API'

  static flags = {
    help: flags.help({char: 'h'})
  }

  static args = [
    {
      name: 'schematic',
      description: 'the schematic type to write the file as',
      required: true,
      options: ['resource', 'r', 'provider', 'p', 'module', 'm']
    },
    {
      name: 'filename',
      description: 'where to write the file',
      required: true
    }
  ]

  async run() {
    const args: any = this.parse(Generate).args
    const schematic: GenerationSchematic = args.schematic
    let filename: string = path.dirname(Fs.GetCliConfig().startingPointFilename) + '/' + args.filename + '.ts'

    await Fs.EnsureDirectory(path.dirname(filename))

    const Name: string = filename.split('/')[filename.split('/').length - 1].split('.')[0]
    const ClassName: string = Name.split('-')
      .map(piece => piece[0].toUpperCase() + piece.slice(1))
      .toString()
      .split(',')
      .join('')

    if (Fs.GetCliConfig().expectedCLIVersion !== Fs.GetCLIVersion()) {
      Logger.Warning("This project's recommended version of the CLI's different from the version of it.")
    }

    if (schematic === 'resource' || schematic === 'r') {
      const RouteClassName = ClassName + 'Resource'

      filename = filename.split('.')[0] + '.resource.ts'

      await Fs.WriteFile(
        filename,
        `import { Resource, Route, Request, Response, Next } from '@symetrical/core';

@Resource('/${Name.toLowerCase()}')
export class ${RouteClassName} {
  @Route({
    path: '', // This kinda route, that has a empty path means that this route will run exatcly inside the Resource path only. If you remove the property path from here, it'll look at the name of the function to make the path.
    method: 'GET' // This is the HTTP method that the route should run on.
  })
  index(req: Request, res: Response, next: Next) {
    res.send('Sup! This is a route inside a resource, right?');
    next();
  }
}`
      )

      await Fs.AddToBootstrapMetadata(RouteClassName, 'resources')
      await Fs.AddImportToStartingPoint(filename.split('/').slice(1).join('/').replace('.ts', ''), RouteClassName)
    } else if (schematic === 'provider' || schematic === 'p') {
      await Fs.WriteFile(
        filename,
        `import { Injectable } from '@symetrical/core';

@Injectable()
export class ${ClassName} {
  constructor() {
    console.info('This provider works!');
  }
}`
      )

      await Fs.AddToBootstrapMetadata(ClassName, 'providers')
      await Fs.AddImportToStartingPoint(filename.split('/').slice(1).join('/').replace('.ts', ''), ClassName)
    } else if (schematic === 'm' || schematic === 'module') {
      const ModuleClassName = ClassName + 'Module'

      filename = filename.split('.')[0] + '.module.ts'

      await Fs.WriteFile(
        filename,
        `import { SMModule } from '@symetrical/core';

@SMModule({
  imports: [],
  providers: [],
  resources: [],
  uses: [],
  preflights: []
})
export class ${ModuleClassName} {
  constructor() {
    console.info('I think this is a Module, dont you?');
  }
}`
      )

      await Fs.AddToBootstrapMetadata(ModuleClassName, 'imports')
      await Fs.AddImportToStartingPoint(filename.split('/').slice(1).join('/').replace('.ts', ''), ModuleClassName)
    }

    Logger.Created(filename)
  }
}
