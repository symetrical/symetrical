import {IBootstrap} from '@symetrical/core'
import * as fs from 'fs'
import * as JSON5 from 'json5'

import * as Logger from './log'

export function IsDirectory(file: string): boolean {
  return !file.includes('.')
}

export function EnsureDirectory(path: fs.PathLike): Promise<void> {
  return new Promise((resolve, reject) => {
    if (!fs.existsSync(path)) {
      CreateDirectory(path)
        .then(resolve)
        .catch(reject)
    } else {
      resolve()
    }
  })
}

export function ReadDirectory(dir: fs.PathLike): Promise<string[]> {
  return new Promise((resolve, reject) => {
    fs.readdir(dir, (err, files) => {
      if (err) reject(err)
      else resolve(files)
    })
  })
}

export function CopyFile(src: fs.PathLike, dest: fs.PathLike): Promise<void> {
  return new Promise((resolve, reject) => {
    fs.copyFile(src, dest, err => {
      if (err) reject(err)
      else resolve()
    })
  })
}

export function CreateDirectory(path: fs.PathLike): Promise<void> {
  return new Promise((resolve, reject) => {
    fs.mkdir(path, err => {
      if (err) reject(err)
      else resolve()
    })
  })
}

export async function CopyDirectory(
  src: fs.PathLike,
  dest: fs.PathLike
): Promise<void> {
  const SourceFiles = await ReadDirectory(src)

  for (const SourceFile of SourceFiles) {
    if (IsDirectory(`${SourceFile}`)) {
      await CreateDirectory(`${dest}/${SourceFile}`)
      await CopyDirectory(`${src}/${SourceFile}`, `${dest}/${SourceFile}`)
    } else {
      await CopyFile(`${src}/${SourceFile}`, `${dest}/${SourceFile}`)
      Logger.Created(`${dest}/${SourceFile}`)
    }
  }
}

export function ReadFile(path: fs.PathLike): Promise<string> {
  return new Promise((resolve, reject) => {
    fs.readFile(path, (err, data) => {
      if (err) reject(err)
      else resolve(data.toString())
    })
  })
}

export async function RepalceInEveryFileOfDirectory(
  search: string,
  value: string,
  directory: fs.PathLike
): Promise<void> {
  const Files = await ReadDirectory(directory)

  for (const filename of Files) {
    if (IsDirectory(filename)) {
      await RepalceInEveryFileOfDirectory(
        search,
        value,
        directory + '/' + filename
      )
    } else {
      const FileData: string = await ReadFile(directory + '/' + filename)
      const NewFileData: string = FileData.split(search).join(value)
      fs.writeFileSync(directory + '/' + filename, NewFileData)
    }
  }
}

export function WriteFile(path: string | number | Buffer | URL, data: any): Promise<void> {
  return new Promise((resolve, reject) => {
    fs.writeFile(path, data, err => {
      if (err) reject(err)
      else resolve()
    })
  })
}

export function GetCLIVersion(): string {
  return '3.7.4'
}

export function GetCliConfig(): {
  startingPointFilename: string;
  expectedCLIVersion: string;
  expectedCoreVersion: string;
} {
  if (!Exists('cli.json')) throw new Error('Could not find CLI config! (more specifically, the "cli.json" file)')

  return JSON.parse(fs.readFileSync('cli.json').toString())
}

export async function GetStartingPointBootstrapMetadata(): Promise<string> {
  const StartingPointFilename = GetCliConfig().startingPointFilename
  let StartingPointFiledata = await ReadFile(StartingPointFilename)

  const BootstrapStartIndex = StartingPointFiledata.indexOf('@Bootstrap({') + 12
  const BootstrapEndIndex = StartingPointFiledata.indexOf('})', BootstrapStartIndex)

  const BootstrapStringfied = StartingPointFiledata.slice(BootstrapStartIndex, BootstrapEndIndex)

  return BootstrapStringfied
}

export async function SetStartingPointBootstrapMetadataTo(metadata: IBootstrap | string): Promise<void> {
  const StartingPointFilename = GetCliConfig().startingPointFilename
  let StartingPointFiledata = await ReadFile(StartingPointFilename)

  const BootstrapStartIndex = StartingPointFiledata.indexOf('@Bootstrap({') + 12
  const BootstrapEndIndex = StartingPointFiledata.indexOf('})', BootstrapStartIndex)

  const Bootstrap = StartingPointFiledata.slice(BootstrapStartIndex, BootstrapEndIndex)

  StartingPointFiledata = StartingPointFiledata.split(Bootstrap).join(`${typeof metadata === 'object' ? JSON5.stringify(metadata) : metadata}`)

  await WriteFile(StartingPointFilename, StartingPointFiledata)
}

export async function AddImportToStartingPoint(path: string, whatToImport: string) {
  const StartingPointFilename = GetCliConfig().startingPointFilename
  let StartingPointFiledata: string | string[] = (await ReadFile(StartingPointFilename)).split('\n')

  StartingPointFiledata[StartingPointFiledata.findIndex((line: string) => line.includes('import'))] += `\nimport { ${whatToImport} } from './${path}';`

  let NewStartingPointFiledata = ''

  StartingPointFiledata.forEach((line: string) => NewStartingPointFiledata += line + '\n')

  await WriteFile(StartingPointFilename, NewStartingPointFiledata)
}

export async function AddToBootstrapMetadata(__class__: string, property: string) {
  const StartingPointBootstrapMetadata = (await GetStartingPointBootstrapMetadata()).split(',\n')

  const PropertyIndex = StartingPointBootstrapMetadata.findIndex(property_ => property_.includes(property))

  if (PropertyIndex > -1) {
    const PropertyItems: string = StartingPointBootstrapMetadata[PropertyIndex].slice(StartingPointBootstrapMetadata[PropertyIndex].indexOf('[') + 1, StartingPointBootstrapMetadata[PropertyIndex].indexOf(']'))
    const PropertyItemsArray: string[] = PropertyItems.split(' ').join('').split(',').filter(item => item !== '')
    PropertyItemsArray.push(__class__)

    Logger.Log(StartingPointBootstrapMetadata[PropertyIndex].slice(StartingPointBootstrapMetadata[PropertyIndex].indexOf('[') + 1, StartingPointBootstrapMetadata[PropertyIndex].indexOf(']')))
    StartingPointBootstrapMetadata[PropertyIndex] = StartingPointBootstrapMetadata[PropertyIndex].replace(StartingPointBootstrapMetadata[PropertyIndex].slice(StartingPointBootstrapMetadata[PropertyIndex].indexOf('['), StartingPointBootstrapMetadata[PropertyIndex].indexOf(']') + 1), '[' + PropertyItemsArray.toString().split(',').join(', ') + ']')
  } else {
    if (StartingPointBootstrapMetadata.length === 0) {
      StartingPointBootstrapMetadata[0] =
        `{
  ${property}: [${__class__}]
}`
    } else {
      StartingPointBootstrapMetadata.push(' ' + property + ': ' + '[' + __class__ + ']')
    }
  }

  await SetStartingPointBootstrapMetadataTo(StartingPointBootstrapMetadata.toString())
}

export function Exists(path: fs.PathLike) {
  return fs.existsSync(path)
}
