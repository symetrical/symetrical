import chalk from 'chalk'
import * as cp from 'child_process'

// tslint:disable: no-console
export function Log(log: any) {
  console.log(log)
}

export function Warning(warning: string) {
  console.warn(`[${chalk.yellowBright(chalk.bold('WARNING'))}] ${warning}`)
}

export function Created(filename: string) {
  console.info(`${chalk.greenBright(chalk.bold('CREATED'))} ${filename}!`)
}

export function RunCommand(command: string) {
  console.info(`> ${command}\n\n`)

  console.info(cp.execSync(command).toString())
}
