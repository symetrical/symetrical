# Symetrical Docs - Getting started

This guide will consist of only 3 steps in which you'll have a new awesome API that works perfectly.

## Prerrequisites

Symetrical is completely based on Node and NPM, even more than based on Typescript.

## Step 1 - Installing the CLI(Command Line Interface)

First, run the command `npm install -g @symetrical/cli`, this will install the latest version of the  CLI.

To see if you already have the CLI installed you can use the command `sm -v`, this will show the Symetrical version in which it's installed.

## Step 2 - Creating the API

Now to create the API using the CLI, you need to run `sm new my-awesome-api`, this will create a API called "my-awesome-api".

## Step 3 - Running the API

To test if the API was correctly created, you can run it.

To do so, you need to compile the Typescript code to Javascript, you can run the command `npm run build`, with that you'll be able to run the API using `npm start`;

As default, the API will run at the port 3000, to test it, you'll need to go to ””“localhost:3000”.

If everything is running corectly, you have an awesome API just yours!

If you found any kind of problem, please create an Issue about that, to do so you can click [here](https://gitlab.com/symetrical/symetrical/issues)
