# Documentação do Symetrical - Começando

Este guia consistirá de apenas 3 passos, nos quais você irá ter uma nova API incrível que funciona perfeitamente.

## Prerrequisitos

Symetrical é completamente baseado em Node e NPM, até mais do que é baseado em Typescript.

## Passo 1 - Instalando o CLI(Command Line Interface)

Primeiro, rode o comando `npm install -g @symetrical/cli`, isto irá instalar a versão mais nova do CLI.

Para ver se você já tem o CLI do framework instalado com a versão certa, rode `sm -v`, isso irá lhe mostrar a versão instalada se o CLI estiver instalado.

## Passo 2 - Criando a API

Agora para criar a API usando o CLI, você precisa rodar `sm new minha-api-incrivel`, isso irá criar uma API chamada de Minha API Incrível.

## Passo 3 - Rodando a API

Para testar se a API foi corretamente criada, você pode rodar a API.

Para fazer isso, você precisa primeiro compilar o código Typescript para Javscript, (pois o Node apenas compreende o código Javascript), você pode fazer tal coisa utilizando o comando `npm run build`; com isso você poderá rodar a API utilizando `npm start`.

Por padrão ela rodará na porta 3000, então para testá-la, você deverá ir no link “localhost:3000”.

Se tudo estiver funcionado corretamente você tem uma API sua funcionando, bom trabalho!

Se você achou qualquer tipo de problema, por favor crie uma Issue sobre isso, para isso clique [aqui](https://gitlab.com/symetrical/symetrical/issues).
