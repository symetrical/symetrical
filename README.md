# Symetrical

## Por quê? | Why?

Symetrical has the purpose of giving a better experience in the process of creating an RESTfull API.

Symetrical têm o propósito de lhe dar uma melhor experiência na criação de API's RESTfull.

### Injeção de dependências | Dependency Injection

Inside Symetrical, one of the best thing you'll encounter, is Dependency Injection.

Everything inside your Symetrical API will be connected, and between these connections, you can make pieces of your API trade information between eachother.

Dentro do Symetrical, uma das melhores coisas que irá encontrar, é a Injeção de dependências.

Tudo dentro de sua API será conectado, então entre essas conexões, você pode fazer partes de sua API trocar informações.

### Flexibilidade | Flexibility

Opposite to most libraries, Symetrical has an extreme flexibility, creating modules, so that other people can use it freely, or you can utilize in another API of yours, only importing it somewhere inside your application.

Ao contrário da maioria das bibliotecas, o Symetrical contém uma extrema flexibilidade, criando módulos dentro do Symetrical, para que outra pessoa possa utilizá-lo, ou você utilizá-lo em outro projeto, apenas importando-o.

### Agilidade | Agility

You can literally create a new API in 5 minutes, or less, it's simple to us. Symetrical utilizes a relatively new language called **Typescript**.

Typescript is easy, flexible, and the best part, it's the base of all Symetrical.

Você pode literalmente criar uma nova API em 5 minutos, talvez até menos, é simples de utilizar-se, e utiliza uma linguagem relativamente nova: Typescript.

Typescript é fácil, flexível, e a melhor parte, é a báse de todo o Symetrical.

## Guia para começar || Starting Guide

You'll find a guide inside the docs of [Symetrical](https://gitlab.com/symetrical/symetrical/blob/master/docs/getting-started-en.md).

Você pode encontrar este guia dentro das docs do [Symetrical](https://gitlab.com/symetrical/symetrical/blob/master/docs/getting-started.md).

## Se quiser ajudar no projeto || If you want to help

You can help find a problem in the Symetrical framework, and then creating Issues to discuss about that problem.

Você pode ajudar achando problemas dentro do Symetrical e criando Issues falando sobre o problema.
